class SessionsController < ApplicationController
  def new
  end

 def create
 	user = User.find_by(email: params[:session][:email].downcase)
 	if user && user.authenticate(params[:session][:password])

 		if user.activated?
	 		#登入用户，然后重定向至用户的资料页面
	 		log_in(user)
	 		params[:session][:remember_me] == '1' ? remember(user) : forget(user)
	 		redirect_back_or user
	 	else
	 		message = "账户未激活. "
	 		message += "请联系管理员激活"
	 		flash[:warning] = message
	 		redirect_to root_url
	 	end
 	else
 		#创建一个错误消息
 		flash.now[:danger] = '邮箱或密码错误'
 		render 'new'
 	end
 end

 def destroy
 	log_out if logged_in?
 	redirect_to root_url
 end
end
